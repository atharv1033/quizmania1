package com.atharv.quizmania;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Calendar;

public class Stats extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);

        TextView text1,text2,text3,text4,text5;

        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        text3 = findViewById(R.id.text3);
        text4 = findViewById(R.id.text4);
        text5 = findViewById(R.id.text5);

        String ans_correctly, ans_wrong, total_attempted, time_taken, average_time;

        Intent intent = getIntent();
        ans_correctly = intent.getStringExtra("ans_correctly");
        ans_wrong = intent.getStringExtra("ans_wrong");
        total_attempted = intent.getStringExtra("total_attempted");
        time_taken = intent.getStringExtra("total_attempted");
        average_time = intent.getStringExtra("average_time");

        text1.setText(text1.getText().toString() +" "+ total_attempted);
        text2.setText(text2.getText().toString() +" "+ ans_correctly);
        text3.setText(text3.getText().toString() +" "+ ans_wrong);
        text4.setText(text4.getText().toString() +" "+ time_taken);
        text5.setText(text5.getText().toString() +" "+ average_time);


    }

    public void go_to_main(View view) {

        startActivity(new Intent(Stats.this, MainActivity.class));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis() + (60 * 60 * 1000));

        AlarmManager alarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(this, AlarmTrigger.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 1, i, 0);

        alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
    }
}
