package com.atharv.quizmania;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.Call;

public class ResponseModel {

    @SerializedName("response_code")
    int response_code;

    @SerializedName("results")
    List<QAModel> results;

}
