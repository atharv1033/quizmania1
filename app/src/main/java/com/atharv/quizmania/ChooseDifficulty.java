package com.atharv.quizmania;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class ChooseDifficulty extends AppCompatActivity {

    String category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_difficulty);

        category = getIntent().getStringExtra("category");

        Toast.makeText(this, "Choose Difficulty", Toast.LENGTH_SHORT).show();
    }

    public void difficulty_clicked(View view) {

        TextView difficulty = (TextView) view;
        String difficulty_str = difficulty.getText().toString();
        String url_link = "";

        if(difficulty_str.equals("Easy")) {
            if(category.equals("Computers")) {
                url_link = "/api.php?amount=10&category=18&difficulty=easy&type=multiple";
            }
            if(category.equals("Sports")) {
                url_link = "/api.php?amount=10&category=21&difficulty=easy&type=multiple";
            }
            if(category.equals("Music")) {
                url_link = "/api.php?amount=10&category=12&difficulty=easy&type=multiple";
            }
            if(category.equals("Film")) {
                url_link = "/api.php?amount=10&category=11&difficulty=easy&type=multiple";
            }
            if(category.equals("Anime and Manga")) {
                url_link = "/api.php?amount=10&category=31&difficulty=easy&type=multiple";
            }
            if(category.equals("Television")) {
                url_link = "/api.php?amount=10&category=14&difficulty=easy&type=multiple";
            }
        }

        if(difficulty_str.equals("Medium")) {

            if(category.equals("Computers")) {
                url_link = "/api.php?amount=10&category=18&difficulty=medium&type=multiple";
            }
            if(category.equals("Sports")) {
                url_link = "/api.php?amount=10&category=21&difficulty=medium&type=multiple";
            }
            if(category.equals("Music")) {
                url_link = "/api.php?amount=10&category=12&difficulty=medium&type=multiple";
            }
            if(category.equals("Film")) {
                url_link = "/api.php?amount=10&category=11&difficulty=medium&type=multiple";
            }
            if(category.equals("Anime and Manga")) {
                url_link = "/api.php?amount=10&category=31&difficulty=medium&type=multiple";
            }
            if(category.equals("Television")) {
                url_link = "/api.php?amount=10&category=14&difficulty=medium&type=multiple";
            }

        }

        if(difficulty_str.equals("Hard")) {

            if(category.equals("Computers")) {
                url_link = "/api.php?amount=10&category=18&difficulty=hard&type=multiple";
            }
            if(category.equals("Sports")) {
                url_link = "/api.php?amount=10&category=21&difficulty=hard&type=multiple";
            }
            if(category.equals("Music")) {
                url_link = "/api.php?amount=10&category=12&difficulty=hard&type=multiple";
            }
            if(category.equals("Film")) {
                url_link = "/api.php?amount=10&category=11&difficulty=hard&type=multiple";
            }
            if(category.equals("Anime and Manga")) {
                url_link = "/api.php?amount=10&category=31&difficulty=hard&type=multiple";
            }
            if(category.equals("Television")) {
                url_link = "/api.php?amount=10&category=14&difficulty=hard&type=multiple";
            }

        }

        //Toast.makeText(this, category, Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, difficulty_str, Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, url_link, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(ChooseDifficulty.this, Question.class);
        intent.putExtra("category", category);
        intent.putExtra("difficulty", difficulty_str);
        intent.putExtra("url_link", url_link);
        startActivity(intent);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis() + (60 * 60 * 1000));

        AlarmManager alarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(this, AlarmTrigger.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 1, i, 0);

        alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
    }

}
