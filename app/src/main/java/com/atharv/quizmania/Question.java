package com.atharv.quizmania;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Question extends AppCompatActivity {

    String category, difficulty, url_link, time_taken;
    Boolean end_quiz = false;
    TextView question_tv, timer_tv;
    RadioButton r1,r2,r3,r4;
    RadioGroup rg;
    CountDownTimer timer;
    Integer question_no = 0, ans_correctly = 0, ans_wrong = 0, total_attempted;
    List<QAModel> questions;
    Long start_time, average_time;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        Intent intent = getIntent();
        category = intent.getStringExtra("category");
        difficulty = intent.getStringExtra("difficulty");
        url_link = intent.getStringExtra("url_link");

        question_tv = findViewById(R.id.question_tv);
        timer_tv = findViewById(R.id.timer_tv);
        r1 = findViewById(R.id.radioButton1);
        r2 = findViewById(R.id.radioButton2);
        r3 = findViewById(R.id.radioButton3);
        r4 = findViewById(R.id.radioButton4);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<ResponseModel> call = service.getQA(url_link);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                questions = response.body().results;
                start_time = System.currentTimeMillis();
                go_next(questions);
                }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(Question.this, "Something went Wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void ans_clicked(View view) {
        RadioButton radioButton = (RadioButton)view;
        if(radioButton.getText().toString().equals(questions.get(question_no).getCorrect_answer())) {
            ans_correctly = ans_correctly + 1;
            Toast.makeText(this, "Right Answer", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, ans_correctly.toString(), Toast.LENGTH_SHORT).show();
        } else {
            ans_wrong = ans_wrong + 1;
            Toast.makeText(this, "Wrong Answer", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, ans_wrong.toString(), Toast.LENGTH_SHORT).show();
        }
        radioButton.setChecked(false);
        timer.onFinish();
    }

    public void skip_question(View view) {
        timer.onFinish();
    }

    public void end_quiz(View view) {
        timer.cancel();

        Long total_time = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - start_time);
        average_time = total_time/10;
        time_taken = total_time.toString();
        total_attempted = ans_correctly + ans_wrong;

        intent = new Intent(Question.this, Stats.class);
        intent.putExtra("ans_correctly", ans_correctly.toString());
        intent.putExtra("ans_wrong", ans_wrong.toString());
        intent.putExtra("total_attempted", total_attempted.toString());
        intent.putExtra("time_taken", time_taken);
        intent.putExtra("average_time", average_time.toString());
        startActivity(intent);
    }

    public void go_next(List<QAModel> list) {

        QAModel q = list.get(question_no);

        question_tv.setText(q.getQuestion());

        ArrayList<String> arrayList = new ArrayList();
        arrayList.add(q.getCorrect_answer());
        arrayList.add(q.getIncorrect_answers().get(0));
        arrayList.add(q.getIncorrect_answers().get(1));
        arrayList.add(q.getIncorrect_answers().get(2));
        Collections.shuffle(arrayList);

        r1.setText(arrayList.get(0));
        r2.setText(arrayList.get(1));
        r3.setText(arrayList.get(2));
        r4.setText(arrayList.get(3));

        timer = new CountDownTimer(30000, 1000) {

            @Override
            public void onTick(long l) {
                timer_tv.setText(new SimpleDateFormat("ss").format(new Date(l)));
            }

            @Override
            public void onFinish() {
                timer.cancel();
                question_no = question_no + 1;
                if(question_no == 10) {
                    Long time_taken = System.currentTimeMillis() - start_time;

                    intent = new Intent(Question.this, Stats.class);
                    intent.putExtra("ans_correctly", ans_correctly.toString());
                    intent.putExtra("ans_wrong", ans_wrong.toString());
                    intent.putExtra("total_attempted", total_attempted.toString());
                    intent.putExtra("time_taken", time_taken);
                    intent.putExtra("average_time", average_time.toString());
                    startActivity(intent);
                }
                go_next(questions);
            }
        };

        timer.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis() + (60 * 60 * 1000));

        AlarmManager alarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(this, AlarmTrigger.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 1, i, 0);

        alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
    }

}
