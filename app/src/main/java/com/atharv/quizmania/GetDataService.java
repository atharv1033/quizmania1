package com.atharv.quizmania;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface GetDataService {

    @GET
    Call<ResponseModel> getQA(@Url String url);

}
